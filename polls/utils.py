import hashlib
import random

import requests
import ujson


def make_sha256_digest(content):
    """生成字符串的SHA256摘要"""
    m = hashlib.sha256()
    m.update(content.encode())
    return m.hexdigest()


def gen_verify_code(length=4):
    """生成指定长度的验证码"""
    all_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    return ''.join(random.choices(all_chars, k=length))


def gen_mobile_code(length=6):
    """生成手机验证码"""
    all_chars = '0123456789'
    return ''.join(random.choices(all_chars, k=length))


def send_mobile_code(tel, code):
    """发送手机验证码"""
    resp = requests.post(
        url='http://sms-api.luosimao.com/v1/send.json',
        auth=('api', 'key-d752503b8db92317a2642771cec1d9b0'),
        data={
            'mobile': tel,
            'message': f'您的短信验证码是{code}，打死也不能告诉别人哟！【Python小课】'
        },
        verify=False
    )
    return ujson.loads(resp.text)
