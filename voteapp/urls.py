"""voteapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include

from polls.views import show_subjects, show_teachers, praise_or_criticize, login, logout, captcha, register, \
    get_mobile_code

urlpatterns = [
    path('', show_subjects),
    path('captcha/', captcha),
    path('mcode/<str:tel>/', get_mobile_code),
    path('login/', login),
    path('register/', register),
    path('logout/', logout),
    path('teachers/', show_teachers),
    path('praise/', praise_or_criticize),
    path('criticize/', praise_or_criticize),
    path('jet/', include('jet.urls', 'jet')),
    path('admin/', admin.site.urls),
]
